# Room for improvement

Code is never perfect, but it can be good enough.

This code is not good enough. Can you find any improvements?

# Setup

Import as gradle project using intellij or eclipse. Or just use your favorite editor.

# Programs

## 1.  Guessing game

The objective is to guess the number the computer is "thinking" of.
Program entry point in __guessinggame.Main__